/*
Navicat SQL Server Data Transfer

Source Server         : SQLSERVER
Source Server Version : 110000
Source Host           : .:1433
Source Database       : dbPixie
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 110000
File Encoding         : 65001

Date: 2014-02-27 14:49:55
*/


-- ----------------------------
-- Table structure for Comment_info
-- ----------------------------
GO
CREATE TABLE [dbo].[Comment_info] (
[id] int NOT NULL IDENTITY(1,1) ,
[username] nvarchar(150) NOT NULL ,
[images_id] int NOT NULL ,
[content] nvarchar(1000) NULL ,
[postdate] datetime NOT NULL 
)


GO

-- ----------------------------
-- Records of Comment_info
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Comment_info] ON
GO
SET IDENTITY_INSERT [dbo].[Comment_info] OFF
GO

-- ----------------------------
-- Table structure for Followers
-- ----------------------------
GO
CREATE TABLE [dbo].[Followers] (
[id] int NOT NULL IDENTITY(1,1) ,
[username] nvarchar(150) NOT NULL ,
[follower_username] nvarchar(150) NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Followers]', RESEED, 4)
GO

-- ----------------------------
-- Records of Followers
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Followers] ON
GO
INSERT INTO [dbo].[Followers] ([id], [username], [follower_username]) VALUES (N'1', N'd2phap', N'duong2duong')
GO
GO
INSERT INTO [dbo].[Followers] ([id], [username], [follower_username]) VALUES (N'2', N'd2phap', N'nqtuan164')
GO
GO
INSERT INTO [dbo].[Followers] ([id], [username], [follower_username]) VALUES (N'3', N'd2phap', N'bocbalui')
GO
GO
INSERT INTO [dbo].[Followers] ([id], [username], [follower_username]) VALUES (N'4', N'nqtuan164', N'bocbalui')
GO
GO
SET IDENTITY_INSERT [dbo].[Followers] OFF
GO

-- ----------------------------
-- Table structure for Images
-- ----------------------------
GO
CREATE TABLE [dbo].[Images] (
[id] int NOT NULL IDENTITY(1,1) ,
[link] nvarchar(255) NOT NULL ,
[username] nvarchar(150) NOT NULL ,
[content] nvarchar(1000) NULL ,
[postdate] datetime NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Images]', RESEED, 2)
GO

-- ----------------------------
-- Records of Images
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Images] ON
GO
INSERT INTO [dbo].[Images] ([id], [link], [username], [content], [postdate]) VALUES (N'2', N'http://abc.com', N'd2phap', N'Bắt đầu một quyết tâm thực hiện', N'2014-02-25 21:36:41.873')
GO
GO
SET IDENTITY_INSERT [dbo].[Images] OFF
GO

-- ----------------------------
-- Table structure for Like_info
-- ----------------------------
GO
CREATE TABLE [dbo].[Like_info] (
[username] nvarchar(150) NOT NULL ,
[images_id] int NOT NULL 
)


GO

-- ----------------------------
-- Records of Like_info
-- ----------------------------

-- ----------------------------
-- Table structure for Users
-- ----------------------------
GO
CREATE TABLE [dbo].[Users] (
[username] nvarchar(150) NOT NULL ,
[password] nvarchar(150) NOT NULL ,
[facebookid] nvarchar(100) NULL ,
[name] nvarchar(100) NULL ,
[about] nvarchar(500) NULL ,
[avatar] nvarchar(255) NULL ,
[type] int NOT NULL DEFAULT ((0)) ,
[activation] int NOT NULL DEFAULT ((1)) 
)


GO

-- ----------------------------
-- Records of Users
-- ----------------------------
INSERT INTO [dbo].[Users] ([username], [password], [facebookid], [name], [about], [avatar], [type], [activation]) VALUES (N'bocbalui', N'5e9795e3f3ab55e7790a6283507c085db0d764fc', N'100001071811768', N'Lộc', null, null, N'0', N'1')
GO
GO
INSERT INTO [dbo].[Users] ([username], [password], [facebookid], [name], [about], [avatar], [type], [activation]) VALUES (N'd2phap', N'8cb2237d0679ca88db6464eac60da96345513964', N'100000190054499', N'Dương Diệu Pháp', N'Đẹp trai, lịch lãm', null, N'1', N'1')
GO
GO
INSERT INTO [dbo].[Users] ([username], [password], [facebookid], [name], [about], [avatar], [type], [activation]) VALUES (N'duong2duong', N'5e9795e3f3ab55e7790a6283507c085db0d764fc', N'830533897', N'Dương', null, null, N'0', N'1')
GO
GO
INSERT INTO [dbo].[Users] ([username], [password], [facebookid], [name], [about], [avatar], [type], [activation]) VALUES (N'nqtuan164', N'5e9795e3f3ab55e7790a6283507c085db0d764fc', N'100000152414610', N'Tuấn', null, null, N'0', N'1')
GO
GO

-- ----------------------------
-- Indexes structure for table Comment_info
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Comment_info
-- ----------------------------
ALTER TABLE [dbo].[Comment_info] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table Followers
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Followers
-- ----------------------------
ALTER TABLE [dbo].[Followers] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table Images
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Images
-- ----------------------------
ALTER TABLE [dbo].[Images] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table Like_info
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Like_info
-- ----------------------------
ALTER TABLE [dbo].[Like_info] ADD PRIMARY KEY ([username], [images_id])
GO

-- ----------------------------
-- Indexes structure for table Users
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Users
-- ----------------------------
ALTER TABLE [dbo].[Users] ADD PRIMARY KEY ([username])
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Comment_info]
-- ----------------------------
ALTER TABLE [dbo].[Comment_info] ADD FOREIGN KEY ([images_id]) REFERENCES [dbo].[Images] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Comment_info] ADD FOREIGN KEY ([username]) REFERENCES [dbo].[Users] ([username]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Followers]
-- ----------------------------
ALTER TABLE [dbo].[Followers] ADD FOREIGN KEY ([username]) REFERENCES [dbo].[Users] ([username]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Followers] ADD FOREIGN KEY ([follower_username]) REFERENCES [dbo].[Users] ([username]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Images]
-- ----------------------------
ALTER TABLE [dbo].[Images] ADD FOREIGN KEY ([username]) REFERENCES [dbo].[Users] ([username]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Like_info]
-- ----------------------------
ALTER TABLE [dbo].[Like_info] ADD FOREIGN KEY ([images_id]) REFERENCES [dbo].[Images] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Like_info] ADD FOREIGN KEY ([username]) REFERENCES [dbo].[Users] ([username]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
