
// Lee Yool

var yUtil = {
	setSession: function(key, val) {
		sessionStorage.setItem(key, val);
	},
	getSession: function(key) {
		//console.log(sessionStorage.getItem(key));
		return sessionStorage.getItem(key);
	},
	clearSession: function() {
		sessionStorage.clear();
	},
	setCookie: function(key, val,exdays) {
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = key + "=" + val + "; " + expires;
	},
	getCookie: function(cname){
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) 
		{
			var c = ca[i].trim();
			if ( c.indexOf(name) == 0 ) return c.substring(name.length,c.length);
		}
		return "";
	},
	clearCookie: function() {
		var cookies = document.cookie;

		for (var i = 0; i < cookies.split(";").length; ++i)
		{
		    var myCookie = cookies[i];
		    var pos = myCookie.indexOf("=");
		    var name = pos > -1 ? myCookie.substr(0, pos) : myCookie;
		    document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
		}
	},
	ajax: function(options) {
	    //options.url = "http://pixie.somee.com/api" + options.url; //HOST
	    options.url = "http://localhost:61608/api" + options.url;
		options.header = "Basic " + options.header;

		var data = {
			status: 401,
			data: {}
		};

		$.ajax({
			url: options.url,
			data: options.data,
			type: options.type,
			contentType: 'text/plain',
			crossDomain: true,
			async: false,
			headers: {
				'Authorization': options.header,
				'Accept': 'application/json',
        		'Content-Type': 'application/json' 
			}, 
			beforeSend: function(xhr) {
				xhr.withCredentials = true; 
				xhr.setRequestHeader("Authorization", options.header);
			},
			success: function(o) {
				//console.log(o);
				data = {
					status: 200,
					data: o
				};
			}, 
			error: function(jqXHR, textStatus, errorThrown) {
				data = {
					status: jqXHR.status,
					data: {}
				};
			}
		});

		return data;
	},
	ajaxWithoutHeader: function(options) {
	    //options.url = "http://pixie.somee.com/api" + options.url; //HOST
	    options.url = "http://localhost:61608/api" + options.url;

		var data = {
			status: 401,
			data: {}
		};

		$.ajax({
			url: options.url,
			data: options.data,
			type: options.type,
			crossDomain: true,
			async: false,
			beforeSend: function(xhr) {
				xhr.withCredentials = true; 
			},
			success: function(o) {
				data = {
					status: 200,
					data: o
				};
			}, 
			error: function(jqXHR, textStatus, errorThrown) {
				data = {
					status: jqXHR.status,
					data: {}
				};
			}
		});

		return data;
	},
	ajaxUpload: function(options, file, post) {
	    //options.url = "http://pixie.somee.com/api" + options.url; //HOST
	    options.url = "http://localhost:61608/api" + options.url;
		options.header = "Basic " + options.header;

		var data = {
			status: 401,
			data: {}
		};

		console.log(file);

		
		if (window.FormData !== undefined) {
			var data = new FormData();
			data.append("image", file);
			data.append("content", post);
			$.ajax({
				type: "POST",
				url: options.url,
				contentType: false,
				processData: false,
				data: data,
				headers: {"Authorization": options.header}, 
				crossDomain: true,
				beforeSend: function(xhr) {
					xhr.withCredentials = true; 
					xhr.setRequestHeader("Authorization", options.header);
				},
				success: function (res) {
					$.each(res, function (i, item) {
						viewModel.uploads.push(item);
					});
				}
			});
		} else {
			alert("your browser sucks!");
		}
		

		// $.ajax({
		// 	url: options.url,
		// 	data: file,
		// 	type: options.type,
		// 	cache: false,
  //   		processData: false, // Don't process the files
  //       	contentType: 'multipart/form-data', 
		// 	crossDomain: true,
		// 	headers: {"Authorization": options.header}, 
		// 	beforeSend: function(xhr) {
		// 		xhr.withCredentials = true; 
		// 		xhr.setRequestHeader("Authorization", options.header);
		// 	},
		// 	success: function(o) {
		// 		console.log(o);
		// 		data = {
		// 			status: 200,
		// 			data: o
		// 		};
		// 	}, 
		// 	error: function(jqXHR, textStatus, errorThrown) {
		// 		data = {
		// 			status: jqXHR.status,
		// 			data: {}
		// 		};
		// 	}
		// });


		// Define a boundary, I stole this from IE but you can use any string AFAIK
	    // var reader = new FileReader();
	    // var fileStream = "";
	    // reader.onload = function(e) {

	    // 	fileStream = e.target.result;
	    // 	console.log(fileStream);

	  //   	$.ajax({
			// 	url: options.url,
			// 	data: {byteArrayIn: fileStream},
			// 	type: options.type,
			// 	cache: false,
	  //       	contentType: 'text/plain', 
			// 	crossDomain: true,
			// 	headers: {"Authorization": options.header}, 
			// 	beforeSend: function(xhr) {
			// 		xhr.withCredentials = true; 
			// 		xhr.setRequestHeader("Authorization", options.header);
			// 	},
			// 	success: function(o) {
			// 		console.log(o);
			// 		data = {
			// 			status: 200,
			// 			data: o
			// 		};
			// 	}, 
			// 	error: function(jqXHR, textStatus, errorThrown) {
			// 		data = {
			// 			status: jqXHR.status,
			// 			data: {}
			// 		};
			// 	}
			// });

			//console.log(r.result);
			// fileStream = r.result;

			// //console.log(fileStream);
			// var boundary = "";
			// boundary += Math.floor(Math.random()*32768);
			// boundary += Math.floor(Math.random()*32768);
			// boundary += Math.floor(Math.random()*32768);
		 //    //var boundary = "---------------------------7da24f2e50046";
		 //    var xhr = new XMLHttpRequest();
		 //    // var body = '--' + boundary + '\r\n'
		 //    //          // Parameter name is "file" and local filename is "temp.txt"
		 //    //          + 'Content-Disposition: form-data; name="file";'
		 //    //          + 'filename="' + file.name + '"\r\n'
		 //    //          // Add the file's mime-type
		 //    //          + 'Content-type: plain/text\r\n\r\n'
		 //    //          + fileStream;
		 //    //          //+ boundary + '--';


		 //    var body = '';
			// body += 'Content-type: multipart/form-data, boundary=' + boundary + '\r\n\r\n';
			// body += '--' + boundary + '\r\n';
			// body += 'content-disposition: form-data; name="submitter"\r\n\r\n';
			// body += 'Henrik Nielsen\r\n';
			// body += '--' + boundary + '\r\n';
			// body += 'content-disposition: form-data ; name="data"; filename="' + file.name + '"';
			// body += 'Content-Type: ' + file.type + '\r\n\r\n';
			// body += fileStream + '\r\n';
			// body += '--' + boundary + '--';

		 //    console.log(body);
		 //    xhr.open("POST", options.url, true);
		 //    xhr.setRequestHeader(
		 //        "Content-type", "multipart/form-data; boundary="+boundary
		 //    );
		 //    xhr.setRequestHeader(
		 //        "Authorization",  options.header
		 //    );
		 //    xhr.onreadystatechange = function ()
		 //    {
		 //        if (xhr.readyState == 4 && xhr.status == 200) {
		 //        	data.status = 200;
		 //        }
		            
		 //    }
		 //    xhr.send(body);
		// }
		// reader.readAsDataURL(file);
		

		return data;
	},

}