define([
	'jquery',
	'underscore',
 	'backbone',
 	'sha1',
 	'base64',
 	'util'
], function ($, _, Backbone, CryptoJS, Base64, yUtil) {
	var User = Backbone.Model.extend({
		suburl: '/TaiKhoan',
		isLogin: function() {
			if (yUtil.getCookie("auth")) {
				//console.log("Cookie: " + yUtil.getCookie("auth"));
				return yUtil.getCookie("auth");
			} else {
				if (yUtil.getSession("auth")) {
					//console.log("Cookie: " + yUtil.getSession("auth"));
					return yUtil.getSession("auth");
				}
			}
			return null;
		},
		getUsername: function() {
			if (yUtil.getCookie("username")) {
				//console.log("Cookie: " + yUtil.getCookie("username"));
				return yUtil.getCookie("username");
			} else {
				if (yUtil.getSession("username")) {
					//console.log("Cookie: " + yUtil.getSession("username"));
					return yUtil.getSession("username");
				}
			}
			return null;
		},
		login: function(options) {
			var that = this;
			var pass = options.password;
			if(options.username != 'facebook') {
				var hash = CryptoJS.SHA1(options.password);
				pass = hash.toString(CryptoJS.enc.Hex);
			}

			var authHeader = this.generateToken(options.username, pass)
			console.log(this.toBase64(authHeader));

			var o = yUtil.ajax({
				url: this.suburl + "/LayThongTin",
				type: 'GET',
				header: authHeader,
				data:  {}
			});

			if(o.status == 200) {
				console.log(o.data);
				yUtil.setCookie("auth", authHeader, 2);
				yUtil.setCookie("username", o.data.Username);
				yUtil.setSession("auth", authHeader);
				yUtil.setSession("username", o.data.Username);
				//yUtil.sét

				return o.data.Username;
			} else {
				console.log(o.status);
				return null;
			}

		},
		register: function(data) {
			var str = "Username=" + data.Username + "&Password=" + this.toSHA1(data.Password) + "&Name=" + data.Name + "&FacebookId=" + data.FacebookId + "&About=&Avatar=&UserType=0&Activation=1";
			console.log(str);
			
			var o = yUtil.ajaxWithoutHeader({
				url: this.suburl + "/DangKyTaiKhoan",
				type: 'POST',
				data:  str
			});

			console.log(o);
		},
		getUserId: function() {
			if (yUtil.getCookie("auth")) {
				var username = this.seperateToken(yUtil.getCookie("auth"));
				return username;
			} else {
				if (yUtil.getSession("auth")) {
					var username = this.seperateToken(yUtil.getSession("auth"));
					return username;
				}
			}
			return null;
		},
		getUserInfo: function() {
			var authHeader = this.isLogin();

			if(authHeader) {
				var o = yUtil.ajax({
					url: this.suburl + "/LayThongTin",
					type: 'GET',
					header: authHeader,
					data:  {}
				});

				if(o.status == 200) {
					return o.data;
				} else {
					return null;
				}
			} else {
				Backbone.history.navigate("signin");
			}
		}, 
		getFollowers: function(){
			var authHeader = this.isLogin();

			if (authHeader) {
				var o = yUtil.ajax({
					url: this.suburl + "/LayDanhSachFollowers",
					type: 'GET',
					header: authHeader,
					data: {}
				});

				if (o.status == 200) {
					return o.data;
				} else {
					return null;
				}
			} else {
				Backbone.history.navigate("signin");
			}
		},
		getFollowing: function () {
			var authHeader = this.isLogin();

			if (authHeader) {
				var o = yUtil.ajax({
					url: this.suburl + "/LayDanhSachFollowing",
					type: 'GET',
					header: authHeader,
					data: {}
				});

				if (o.status == 200) {
					return o.data;
				} else {
					return null;
				}
			} else {
				Backbone.history.navigate("signin");
			}
		},
		getUserImages: function () {
		    var authHeader = this.isLogin();

		    if (authHeader) {
		        var o = yUtil.ajax({
		            url: this.suburl + "/LayDanhSachAnh",
		            type: 'GET',
		            header: authHeader,
		            data: {
		                offset: 1,
                        limit: 20
		            }
		        });

		        if (o.status == 200) {
		            return o.data;
		        } else {
		            return null;
		        }
		    } else {
		        Backbone.history.navigate("signin");
		    }
		},
		getImage: function (id) {
		    var authHeader = this.isLogin();

		    if (authHeader) {
		        var o = yUtil.ajax({
		            url: this.suburl + "/LayThongTinAnh",
		            type: 'GET',
		            header: authHeader,
		            data: {
		                id: id
		            }
		        });

		        if (o.status == 200) {
		            return o.data;
		        } else {
		            return null;
		        }
		    } else {
		        Backbone.history.navigate("signin");
		    }
		},
		getComments: function (id) {
		    var authHeader = this.isLogin();

		    if (authHeader) {
		        var o = yUtil.ajax({
		            url: this.suburl + "/LayDanhSachBinhLuanCuaAnh",
		            type: 'GET',
		            header: authHeader,
		            data: {
		                images_id: id,
		                offset: 1,
                        limit: 20
		            }
		        });

		        if (o.status == 200) {
		            return o.data;
		        } else {
		            return null;
		        }
		    } else {
		        Backbone.history.navigate("signin");
		    }
		},
		doComment: function (images_id, content, postdate) {
		    var authHeader = this.isLogin();

		    if (authHeader) {
		        var o = yUtil.ajax({
		            url: this.suburl + "/BinhLuanAnh?images_id=" + images_id + 
                        "&content=" + content + "&postdate=" + postdate,
		            type: 'POST',
		            header: authHeader,
		            data: {
		                
		            }
		        });

		        if (o.status == 200) {
		            return o.data;
		        } else {
		            return null;
		        }
		    } else {
		        Backbone.history.navigate("signin");
		    }
		},
		doLike: function (images_id) {
		    var authHeader = this.isLogin();

		    if (authHeader) {
		        var o = yUtil.ajax({
		            url: this.suburl + "/LikeAnh?images_id=" + images_id,
		            type: 'POST',
		            header: authHeader,
		            data: {}
		        });

		        if (o.status == 200) {
		            //console.log(o.data);

		            if(o.data == -1)
		            {
		                var u = yUtil.ajax({
		                    url: this.suburl + "/UnlikeAnh?images_id=" + images_id,
		                    type: 'GET',
		                    header: authHeader,
		                    data: {}
		                });

		                if (u.status == 200) {
		                    //console.log(o.data);
		                    return u.data;

		                } else {
		                    return null;
		                }
		            }
		            else
		            {
		                return o.data;
		            }

		        } else {
		            return null;
		        }
		    } else {
		        Backbone.history.navigate("signin");
		    }
		},
		updateInfo: function(options) {
			var authHeader = this.isLogin();
			console.log(options);

			//var data = "Name=" + options.Name + "&About=" + options.About;
			//console.log(data);
			var data = JSON.stringify(options);
			console.log(data);

			yUtil.ajax({
				data: data,
				type: 'POST',
				url: this.suburl + '/CapNhatThongTin',
				header: authHeader
			});
		},
		toSHA1: function(str) {
			var hash = CryptoJS.SHA1(str);
			return hash.toString(CryptoJS.enc.Hex);
		},
		toBase64: function(str) {
			return Base64.encode(str);
		},
		toUTF8: function(str) {
			return Base64.decode(str);
		},
		generateToken: function(username, password) {
			return this.toBase64(username + ":" + password);
		},
		seperateToken: function(str) {
			var data = this.toUTF8(str);
			var username = data.split(':');
			return username[0].trim();
		}
		
	});

	return User;
});