﻿/*
 * Router
 */

define([
	'jquery',
	'underscore',
 	'backbone',
    'facebook',
    'util',
    'views/welcome-view',
    'views/login-view',
    'views/signup-view',
    'views/main-view',
    'views/profile-view',
    'views/post-view',
    'views/details-view',
    'views/setting-view',
    'views/upload-view'
], function ($, _, Backbone, FB, yUtil, WelcomeView, LoginView, SignupView, MainView, ProfileView, PostView, DetailsView, SettingView, UploadView) {
    var AppRoute = Backbone.Router.extend({
        routes: {
            '': 'home',
            'signin': 'loginForm',
            'signup': 'signupForm',
            ':username/setting': 'accountSetting',
            ':username/posts': 'viewPosts',
            ':username/post/:id': 'viewImage',
            ':username/upload': 'uploadPost',
            'logout': 'logout',
        }
    });

    var initialize = function () {
        FB.init({
            appId: '1376641262596415'
        });
        
        console.log('We are at Router');
        var router = new AppRoute();
        var mainView = new MainView();

        router.on('route:home', function () {
            var welcomeView = new WelcomeView();
            welcomeView.render();
        });

        router.on('route:loginForm', function () {
            var loginView = new LoginView();
            loginView.render();
        });

        router.on('route:signupForm', function () {
            var signupView = new SignupView();
            signupView.render();
        });
        router.on('route:logout', function () {
            yUtil.clearSession();
            yUtil.clearCookie();
            Backbone.history.navigate("", {trigger: true});
        });
        router.on('route:viewPosts', function(username) {
            console.log(username);
            var profileView = new ProfileView();
            var postView = new PostView();
            mainView.render();
            console.log("Main already render");
            profileView.render();
            postView.render();
        });

        router.on('route:viewImage', function(username, id) {
            console.log(username);
            console.log(id);
            var profileView = new ProfileView();
            var detailsView = new DetailsView();

            mainView.render();
            profileView.render();
            detailsView.render({username: username, id: id});
        });

        router.on('route:accountSetting', function(username) {
            console.log(username);
            var profileView = new ProfileView();
            var settingView = new SettingView();

            mainView.render();
            profileView.render();
            settingView.render({username: username});
        });

        router.on('route:uploadPost', function(username) {
            console.log("Upload by " + username);
            var profileView = new ProfileView();
            var uploadView = new UploadView();
            mainView.render();
            profileView.render();
            uploadView.render();
        });
        Backbone.history.start();
    }
    return {
        initialize: initialize
    };
})
