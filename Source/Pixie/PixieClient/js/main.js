﻿// Filename: main.js

// Require.js allows us to configure shortcut alias
// There usage will become more apparent further along in the tutorial.
require.config({
    paths: {
        jquery: 'libs/jquery',
        underscore: 'libs/underscore',
        backbone: 'libs/backbone',
        templates: '../templates',
        facebook: '//connect.facebook.net/en_US/all',
        sha1: 'libs/sha1',
        base64: 'libs/base64',
        util: 'libs/yUtil'
    },

    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        },
        facebook: {
            exports: 'FB'
        },
        sha1: {
            exports: 'CryptoJS'
        },
        base64: {
            exports: 'Base64'
        },
        util: {
            exports: 'yUtil'
        }
    }
});
require(['fb']);
require([
  // Load our app module and pass it to our definition function
  'app'
], function (App) {
    // The "app" dependency is passed in as "App"
    console.log("We are in Main");
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    // $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
    //     options.crossDomain = {
    //         crossDomain: true
    //     };
    //     options.xhrFields = {
    //         withCredentials: true
    //     };
        
    //     if(sessionStorage.getItem("authHeader")) {
    //         var authHeader = "Basic " + sessionStorage.getItem("authHeader");
    //         console.log(authHeader);
    //         options.headers = {
    //             'Authorization' : authHeader
    //         };
    //         jqXHR.setRequestHeader('Authorization', authHeader);
    //     }
    //     options.url = 'http://localhost:61608/api' + options.url;
    //     // console.log(options);
    //     // console.log(originalOptions);
    //     // console.log(jqXHR);
    // });

    App.initialize();

});