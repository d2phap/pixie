define([
	'jquery',
	'underscore',
 	'backbone',
 	'facebook', 
 	'models/user',
 	'text!templates/image-list-view.html',
], function ($, _, Backbone, FB, User, ImageListViewTemplate) {
	var PostsView = Backbone.View.extend({
		render: function() {
			var user = new User();
			if (user.isLogin()) {
			    Backbone.history.navigate(user.getUsername() + "/posts");
			} else {
				this.$el.html(WelcomeViewTemplate);
			}

			var userImages = user.getUserImages();
			var username = user.getUsername();

             // Do du lieu vo ne
			$("#content").html(_.template(ImageListViewTemplate, {
			    user: {
			        userImages: userImages,
			        username: username
			    }
			}));
		}
	});

	return PostsView;
});