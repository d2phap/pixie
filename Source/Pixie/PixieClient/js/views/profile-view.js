define([
	'jquery',
	'underscore',
 	'backbone',
 	'facebook', 
 	'util',
 	'models/user',
 	'text!templates/profile-view.html',
], function ($, _, Backbone, FB, yUtil, User, ProfileViewTemplate) {
	var ProfileView = Backbone.View.extend({
		render: function() {
			var user = new User();
			if (user.isLogin()) {
			    var userInfo = user.getUserInfo();
			    var followers = user.getFollowers();
			    var followings = user.getFollowing();

			    var followersCount = _.size(user.getFollowers());
			    var followingsCount = _.size(user.getFollowing());
			    //console.log(feeds);

                // Do du lieu vo ne
			    $("#profile").html(_.template(ProfileViewTemplate, {
			        user: {
			            userInfo: userInfo,
			            followersCount: followersCount,
			            followingsCount: followingsCount,
			            followers: followers,
			            followings: followings
			        }
			    }));
			} else {
				Backbone.history.navigate("", {trigger : true});
			}
		}, 
		events: {
			'click #btn-logout': 'logout'
		},
		logout: function() {
			yUtil.clearSession();
			yUtil.clearCookie();
			console.log("logout");
			return false;
		}
	});

	return ProfileView;
});