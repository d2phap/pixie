﻿define([
	'jquery',
	'underscore',
 	'backbone',
 	'facebook',
 	'models/user',
 	'text!templates/login-view.html'
], function ($, _, Backbone, FB, User, LoginViewTemplate) {
	var LoginView = Backbone.View.extend({
		el: '#app',
		render: function() {
			this.$el.html(LoginViewTemplate);
		}, 
		events: {
			'click #btn-signin': 'authentication',
			'click #btn-signin-facebook': 'FBauthentication'
		},
		authentication: function() {
			var username = $("#txtLoginUsername").val();
			var password = $("#txtLoginPassword").val();
			var user = new User();

			var o = user.login({ username: username, password: password });
			console.log(o);
			if (o) {
				var name = user.getUsername();
				Backbone.history.navigate(name + "/posts", {trigger : true});
				$(".alert-success").html("Login success!").fadeIn("slow").delay(1000).fadeOut("slow");
			} else {

			}

			return false;
		},
		FBauthentication: function() {
			console.log('We are in FB-auth');
			var user = new User();
			FB.login(function(response) {
        		FB.api('/me', function(response) {
        		    var o = user.login({ username: 'facebook', password: response.id });
        			if (o) {
        				var name = user.getUsername();
        				console.log(name + "/posts");
						Backbone.history.navigate(name + "/posts", {trigger : true});
						$(".alert-success").html("Login success!").fadeIn("slow").delay(1000).fadeOut("slow");
					} else {

					}
        		});
			}, { scope: 'publish_stream' });
			return false;
		}
	});

	return LoginView;
});