define([
	'jquery',
	'underscore',
 	'backbone',
 	'facebook', 
 	'models/user',
 	'text!templates/main-view.html',
], function ($, _, Backbone, FB, User, MainViewTemplate) {
	var MainView = Backbone.View.extend({
		el: '#app',
		render: function() {
			this.$el.html(MainViewTemplate);
		}
	});

	return MainView;
});