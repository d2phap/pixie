define([
	'jquery',
	'underscore',
 	'backbone',
 	'facebook', 
 	'util',
 	'models/user',
 	'text!templates/upload-view.html',
], function ($, _, Backbone, FB, yUtil, User, UploadViewTemplate) {
	var UploadView = Backbone.View.extend({
		file: null,
		mainele: '.upload-img',
		render: function() {
			var that = this;
			var user = new User();
			if (user.isLogin()) {
				$("#content").html(UploadViewTemplate);
			} else {
				Backbone.history.navigate("", {trigger : true});
			}


			if (window.File && window.FileList && window.FileReader) {
				$('.upload-img').on("dragover", _.bind(that.dragOverEvent, this));
				$('.upload-img').on("dragenter", _.bind(this.dragEnterEvent, this))
				$('.upload-img').on("dragleave", _.bind(that.dragLeaveEvent, this));
				$('.upload-img').on("drop", _.bind(that.dropEvent, this));
				this.draghoverClassAdded = false
			}

			$("#btnPostImage").on('click', _.bind(that.uploadFile, this));
		}, 
		dragOverEvent: function (e) {
			if (e.originalEvent) e = e.originalEvent;
			var data = this.getCurrentDragData(e);
			
			if (this.dragOver(data, e.dataTransfer, e) !== false) {
				if (e.preventDefault) e.preventDefault();
				e.dataTransfer.dropEffect = 'copy'; // default
			}
		},
		dragEnterEvent: function (e) {
			if (e.originalEvent) e = e.originalEvent;
			if (e.preventDefault) e.preventDefault();
		},
		dragLeaveEvent: function (e) {
			if (e.originalEvent) e = e.originalEvent;
			var data = this.getCurrentDragData(e);
			this.dragLeave(data, e.dataTransfer, e);
		}, 
		dropEvent: function (e) {
			if (e.originalEvent) e = e.originalEvent;
			var data = this.getCurrentDragData(e);
			
			if (e.preventDefault) e.preventDefault();
			if (e.stopPropagation) e.stopPropagation(); // stops the browser from redirecting
	 
			if (this.draghoverClassAdded) $(this.mainele).removeClass("draghover");
	 
			this.drop(data, e.dataTransfer, e);
		},
		getCurrentDragData: function (e) {
			var data = null;
			data = e.target.files || e.dataTransfer.files;
			return data;
		},
		dragOver: function (data, dataTransfer, e) { // optionally override me and set dataTransfer.dropEffect, return false if the data is not droppable
			$(this.mainele).addClass("draghover");
			this.draghoverClassAdded = true;
		},
	 
		dragLeave: function (data, dataTransfer, e) { // optionally override me
			if (this.draghoverClassAdded) $(this.mainele).removeClass("draghover");
		},
	 
		drop: function (data, dataTransfer, e) {
			console.log(data);
			console.log(dataTransfer);
			console.log(e);

			if (data[0].type.indexOf("image") == 0) {
				var reader = new FileReader();
				reader.onloadend = function(e) {
					$(".upload-img").html('<img src="' + e.target.result + '" />');
					//console.log(reader.result);
				}
				//console.log(file[0].getAsBinary());
				reader.readAsDataURL(data[0]);
				file = data[0];
			}
			
		}, 
		uploadFile: function() {
			// var url = 'http://localhost:61608/api/TaiKhoan/UploadAnh'
			// var xhr = new XMLHttpRequest();
			console.log("upload");


			var post = $("#txtPost").val();

			var user = new User();
			var authHeader = user.isLogin();

			if (authHeader) {

				if(file != null) {

				}

				var o = yUtil.ajaxUpload({
					url: "/TaiKhoan/UploadAnh",
					type: 'POST',
					header: authHeader
				}, file, post);

				if (o.status == 200) {
					return o.data;
				} else {
					return null;
				}
			} else {
				Backbone.history.navigate("signin", { trigger : true });
			}
			return false;
		}
	});

	return UploadView;
});