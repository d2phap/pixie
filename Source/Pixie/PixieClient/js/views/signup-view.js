define([
	'jquery',
	'underscore',
 	'backbone',
 	'facebook',
 	'sha1',
 	'util',
 	'models/user',
 	'text!templates/signup-view.html'
], function ($, _, Backbone, FB, CryptoJS, yUtil, User, SingupViewTemplate) {
	var SignupView = Backbone.View.extend({
		el: '#app',
		suburl: '/TaiKhoan',
		render: function() {
			this.$el.html(SingupViewTemplate);
		}, 
		events: {
			'click #btn-signup': 'register',
			'click #btn-signup-facebook': 'FBregister'
		},
		register: function() {
			console.log('We are in self-auth');

			var username = $("#txtSignupUsername").val();
			var password = $("#txtSignupPassword").val();
			var passwordConfirm = $("#txtSignupConfirmPassword").val();
			var name = $("#txtSignupName").val();
			var facebookId = $("#txtSignupFacebookId").val();

			if (password == passwordConfirm) {
				var user = new User();
				var data = {
					Username: username,
					Password: password,
					FacebookId: facebookId,
					Name: name,
					About: "",
					Avatar: "",
					UserType: 0,
					Activation: 1
				};
				
				var user = new User();
				user.register(data);
			}

			$(".alert-success").html("Register success!").fadeIn("slow").delay(2000).fadeOut("slow");
			Backbone.history.navigate('signin', {trigger: true});

			return false;
		},
		FBregister: function() {
			//console.log('We are in FB-auth');
			var facebookUser = null;
			FB.getLoginStatus(function(response) {
	            if (response.status == 'connected') {
	            	console.log('Logged in');
	            } else {
	            	FB.login(function(response) {
	            		FB.api('/me', function(response) {
	            			console.log(response);
	            			$("#txtSignupUsername").val(response.username);
	            			$("#txtSignupFacebookId").val(response.id);
	            			$("#txtSignupName").val(response.name);
	            			$("#btn-signup-facebook").html("Connected!").prop("disabled", true);
	            		});
	            	}, { scope: 'publish_stream' });
	            }

	            FB.api('/me', function(response) {
        			console.log(response);
        			$("#txtSignupUsername").val(response.username);
        			$("#txtSignupFacebookId").val(response.id);
        			$("#txtSignupName").val(response.name);
	            	$("#btn-signup-facebook").html("Connected!").prop("disabled", true);
        		});
	        });
			return false;
		}

	});

	return SignupView;
});