define([
	'jquery',
	'underscore',
 	'backbone',
 	'facebook',
 	'models/user',
 	'text!templates/details-view.html'
], function ($, _, Backbone, FB, User, DetailViewTemplate) {
    var DetailsView = Backbone.View.extend({
        el: '#app',
        render: function (options) {
            var user = new User();
            var isLogin = user.isLogin();
            var image = user.getImage(options.id);
            var userInfo = user.getUserInfo();
            var comments = user.getComments(options.id);

            //console.log("----------------------");
            //console.log(isLogin);

            $("#content").html(_.template(DetailViewTemplate, {
                user: {
                    image: image,
                    isLogin: isLogin,
                    postid: options.id,
                    userInfo: userInfo,
                    comments: comments
                }
            }));
        },
        events: {
            'click #btnSubmitComment': 'doComment',
            'click #btnLike': 'doLike'
        },
        doComment: function () {
            var images_id = $("#hidImageId").val();
            var content = $("#txtComment").val();
            var user = new User();
            var link = window.location.href;
            //link = "http://imageglass.org"; //for test
            //console.log(link);

            var cmtCount = user.doComment(images_id, content);

            //console.log("---------------------");
            //console.log(cmtCount);

            var opts = {
                message: content,
                name: 'Pixie - Mạng xã hội hình ảnh',
                link: link
            };

            FB.api('/me/feed', 'post', opts, function (response) {
                if (!response || response.error) {
                    //alert('You must login Facebook');
                    console.log('Dang tai that bai');
                    console.log(response.error);
                }
                else
                {
                    console.log('Dang tai thanh cong');
                }
            });

            return false;
        },
        doLike: function () {
            var images_id = $("#hidImageId").val();
            var user = new User();
            var likeCount = user.doLike(images_id);
            //console.log(likeCount);

            var isLoved = $("#btnLike").attr("data-status");
            if (isLoved == "loved")
            {
                //Unlike
                $("#btnLike").removeClass('loved');
                $("#btnLike").html("<span class='like-count'>" + likeCount + "</span>");
                $("#btnLike").attr("data-status", "");
            }
            else
            {
                //Like
                $("#btnLike").addClass('loved');
                $("#btnLike").html("<span class='like-count'>" + likeCount + "</span>");
                $("#btnLike").attr("data-status", "loved");

                var link = window.location.href;
                // link = 'http://imageglass.org';
                var opts = {
                    message: 'I like it',
                    name: '',
                    link: link
                };

                FB.api('/me/feed', 'post', opts, function (response) {
                    if (!response || response.error) {
                        //alert('You must login Facebook');
                        console.log('Dang tai that bai');
                        console.log(response.error);
                    }
                    else {
                        console.log('Dang tai thanh cong');
                    }
                });
            }
            
            return false;
        }

    });

    return DetailsView;
});;