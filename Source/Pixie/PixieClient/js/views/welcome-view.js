define([
	'jquery',
	'underscore',
 	'backbone',
 	'facebook', 
 	'models/user',
 	'text!templates/welcome-view.html'
], function ($, _, Backbone, FB, User, WelcomeViewTemplate) {
	var WelcomeView = Backbone.View.extend({
		el: '#app',
		render: function() {
			console.log('We are at Home');
			var user = new User();
			if (user.isLogin()) {
				Backbone.history.navigate(user.getUsername() + "/posts", {trigger : true})
			} else {
				this.$el.html(WelcomeViewTemplate);
			}
		},
		events: {
			'click #btn-login': 'login',
			'click #btn-signup': 'signup'
		},

		login: function() {
			Backbone.history.navigate('signin', {trigger: true});
		}, 

		signup: function() {
			Backbone.history.navigate('signup', {trigger: true});
		}
	});

	return WelcomeView;
});