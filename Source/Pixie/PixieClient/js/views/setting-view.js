define([
	'jquery',
	'underscore',
 	'backbone',
 	'facebook', 
 	'models/user',
 	'text!templates/setting-view.html'
], function ($, _, Backbone, FB, User, SettingViewTemplate) {
	var SettingView = Backbone.View.extend({
		suburl: '/TaiKhoan',
		render: function(options) {
			var that = this;
			var user = new User();
			if (user.isLogin()) {
				
			} else {
				Backbone.history.navigate("", {trigger : true});
			}

			var info = user.getUserInfo();
			console.log(info);
			$("#content").html(_.template(SettingViewTemplate, info));
			$('#btnSaveInfo').on('click', _.bind(that.saveInfo, this))
		},
		saveInfo: function() {
			console.log("We are in Setting");
			var user = new User();
			var data = {
				Name : $("#txtName").val(),
				About : $("#txtAboutMe").val()
			}

			user.updateInfo(data);


			return false;
		}

	});

	return SettingView;
});