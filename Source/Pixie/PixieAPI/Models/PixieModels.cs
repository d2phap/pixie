﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace PixieAPI.Models
{
    /// <summary>
    /// Loại người dùng
    /// </summary>
    public enum UserTypeEnum
    {
        RegisteredUser = 0,
        Administrator = 1        
    }

    public class UsersModels
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string FacebookId { get; set; }
        public string About { get; set; }
        public string Avatar { get; set; }
        public UserTypeEnum UserType { get; set; }
        public int Activation { get; set; }
    }

    public class ImagesModels
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public UsersModels User { get; set; }
        public string Content { get; set; }
        public DateTime PostDate { get; set; }
        public int LikeCount { get; set; }
        public int CommentCount { get; set; }
        public bool IsLiked { get; set; }
    }

    public class CommentsModels
    {
        public int Id { get; set; }
        public int ImageId { get; set; }
        public UsersModels User { get; set; }
        public string Content { get; set; }
        public DateTime PostDate { get; set; }
    }

    public class FileDesc
    {
        public string name { get; set; }
        public string path { get; set; }
        public long size { get; set; }
        public FileDesc(string n, string p, long s)
        {
            name = n;
            path = p;
            size = s;
        }
    }

    public static class FunctionPixie
    {
        /// <summary>
        /// Mã hoá ký tự bằng thuật toán SHA1
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string SHA1(string inputString)
        {
            //Mã hoá bằng SHA1
            SHA1Managed sha1 = new SHA1Managed();

            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            var hashedBytes = sha1.ComputeHash(bytes);

            StringBuilder hashedBuilder = new StringBuilder(40);
            for (int i = 0; i < hashedBytes.Length; i++)
            {
                hashedBuilder.Append(hashedBytes[i].ToString("x2"));
            }

            //Chuỗi sau khi mã hoá
            return hashedBuilder.ToString();
        }
    }


}