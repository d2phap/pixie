﻿using PixieAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace PixieAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/TaiKhoan")]
    public class TaiKhoanController : ApiController
    {
        //DataContext để truy vấn LINQ
        private dbPixieDataContext db = new dbPixieDataContext();


        [Route("LayThongTin")]
        [HttpGet, AcceptVerbs("GET")]
        public UsersModels LayThongTin()
        {
            UsersModels user = new UsersModels();
            User u = new User();
            u = (from b in db.Users
                    where b.username == HttpContext.Current.User.Identity.Name
                    select b).SingleOrDefault();
            
            user.Username = u.username;
            user.Name = u.name;
            user.About = u.about;
            user.Avatar = u.avatar;
            user.UserType = (UserTypeEnum) u.type;
            user.Activation = u.activation;

            return user;
        }

        [Route("LayDanhSachFollowers")]
        [HttpGet, AcceptVerbs("GET")]
        public List<UsersModels> LayDanhSachFollowers()
        {
            List<User> followers = db.Followers
                .Where(f => f.username.ToLower().CompareTo(HttpContext.Current.User.Identity.Name) == 0)
                .Select(f => f.User1)
                .ToList();

            List<UsersModels> ds = new List<UsersModels>();
            foreach(var item in followers)
            {
                UsersModels u = new UsersModels();
                u.Username = item.username;
                u.Name = item.name;
                u.About = item.about;
                u.Avatar = item.avatar;
                u.UserType = (item.type == 1) ? UserTypeEnum.Administrator : UserTypeEnum.RegisteredUser;
                u.Activation = item.activation;

                ds.Add(u);
            }

            return ds;
        }

        [Route("LayDanhSachFollowing")]
        [HttpGet, AcceptVerbs("GET")]
        public List<UsersModels> LayDanhSachFollowing()
        {
            List<User> followers = db.Followers
                .Where(f => f.follower_username.ToLower().CompareTo(HttpContext.Current.User.Identity.Name) == 0)
                .Select(f => f.User)
                .ToList();

            List<UsersModels> ds = new List<UsersModels>();
            foreach (var item in followers)
            {
                UsersModels u = new UsersModels();
                u.Username = item.username;
                u.Name = item.name;
                u.About = item.about;
                u.Avatar = item.avatar;
                u.UserType = (item.type == 1) ? UserTypeEnum.Administrator : UserTypeEnum.RegisteredUser;
                u.Activation = item.activation;

                ds.Add(u);
            }

            return ds;
        }

        /// <summary>
        /// Lấy danh sách ảnh mới nhất của bạn bè
        /// </summary>
        /// <param name="offset">Vị trí bắt đầu (mục đích phân trang)</param>
        /// <param name="limit">Số lượng ảnh cần lấy</param>
        /// <returns></returns>
        [Route("LayDanhSachAnhCuaFriends")]
        [HttpGet, AcceptVerbs("GET")]
        public List<ImagesModels> LayDanhSachAnhCuaFriends(int offset, int limit)
        {
            List<ImagesModels> images = new List<ImagesModels>();

            if (offset < 1)
            {
                offset = 1;
            }
            if (limit < 0)
            {
                limit = 0;
            }

            var ds = (from i in db.Images
                      join f in db.Followers
                      on i.username equals f.follower_username
                      join u in db.Users
                      on f.username equals u.username
                      where u.username == User.Identity.Name
                      select new
                      {
                          i.User,
                          i.id,
                          i.link,
                          i.content,
                          i.postdate
                      }).Skip((offset - 1) * limit)
                     .Take(limit)
                     .ToList();

            foreach(var i in ds)
            {
                ImagesModels img = new ImagesModels();
                img.Id = i.id;
                img.Link = i.link;
                img.Content = i.content;
                img.PostDate = i.postdate;

                UsersModels u = new UsersModels();
                u.Username = i.User.username;
                u.Name = i.User.name;
                u.Avatar = i.User.avatar;
                u.About = i.User.about;
                u.UserType = (i.User.type == 1) ? UserTypeEnum.Administrator : UserTypeEnum.RegisteredUser;
                u.Activation = i.User.activation;

                img.User = u;

                int likeCount = (from l in db.Like_infos
                                 where l.images_id == i.id
                                 select l.username
                                 ).Count();

                int commentCount = (from c in db.Comment_infos
                                 where c.images_id == i.id
                                 select c.username
                                 ).Count();

                var likedImage = db.Like_infos
                .SingleOrDefault(v => v.username.CompareTo(User.Identity.Name) == 0 &&
                    v.images_id == i.id);

                bool isLiked = true;
                if (likedImage == null)
                {
                    isLiked = false;
                }

                img.LikeCount = likeCount;
                img.CommentCount = commentCount;
                img.IsLiked = isLiked;

                images.Add(img);
            }

            return images;
        }

        /// <summary>
        /// Lấy danh sách ảnh mới nhất của người dùng
        /// </summary>
        /// <param name="offset">Vị trí bắt đầu (mục đích phân trang)</param>
        /// <param name="limit">Số lượng ảnh cần lấy</param>
        /// <returns></returns>
        [Route("LayDanhSachAnh")]
        [HttpGet, AcceptVerbs("GET")]
        public List<ImagesModels> LayDanhSachAnh(int offset, int limit)
        {
            List<ImagesModels> images = new List<ImagesModels>();

            if (offset < 1)
            {
                offset = 1;
            }
            if (limit < 0)
            {
                limit = 0;
            }

            List<Image> ds = db.Images
                .Where(i => i.username.ToLower().CompareTo(HttpContext.Current.User.Identity.Name) == 0)
                .Skip((offset - 1) * limit).Take(limit).ToList();

            foreach (var i in ds)
            {
                ImagesModels img = new ImagesModels();
                img.Id = i.id;
                img.Link = i.link;
                img.Content = i.content;
                img.PostDate = i.postdate;

                UsersModels u = new UsersModels();
                u.Username = i.User.username;
                u.Name = i.User.name;
                u.Avatar = i.User.avatar;
                u.About = i.User.about;
                u.UserType = (i.User.type == 1) ? UserTypeEnum.Administrator : UserTypeEnum.RegisteredUser;
                u.Activation = i.User.activation;

                img.User = u;

                int likeCount = (from l in db.Like_infos
                                 where l.images_id == i.id
                                 select l.username
                                 ).Count();

                int commentCount = (from c in db.Comment_infos
                                    where c.images_id == i.id
                                    select c.username
                                 ).Count();

                var likedImage = db.Like_infos
                .SingleOrDefault(v => v.username.CompareTo(User.Identity.Name) == 0 &&
                    v.images_id == i.id);

                bool isLiked = true;
                if (likedImage == null)
                {
                    isLiked = false;
                }

                img.LikeCount = likeCount;
                img.CommentCount = commentCount;
                img.IsLiked = isLiked;

                images.Add(img);
            }


            return images;
        }

        /// <summary>
        /// Lấy thông tin chi tiết của tấm ảnh
        /// </summary>
        /// <param name="id">ID của tấm ảnh</param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("LayThongTinAnh")]
        [HttpGet, AcceptVerbs("GET")]
        public ImagesModels LayThongTinAnh(int id)
        {
            ImagesModels image = new ImagesModels();
            Image img = new Image();
            img = (from b in db.Images
                   where b.id == id
                   select b).SingleOrDefault();
            image.Id = img.id;
            image.Link = img.link;
            image.Content = img.content;
            image.PostDate = img.postdate;

            UsersModels user = new UsersModels();
            user.Name = img.User.name;
            user.Username = img.User.username;
            user.UserType = (img.User.type == 1) ? UserTypeEnum.Administrator : UserTypeEnum.RegisteredUser;
            user.Avatar = img.User.avatar;
            user.About = img.User.about;
            user.Activation = img.User.activation;

            image.User = user;

            int likeCount = (from l in db.Like_infos
                             where l.images_id == id
                             select l.username
                                 ).Count();

            int commentCount = (from c in db.Comment_infos
                                where c.images_id == id
                                select c.username
                             ).Count();

            var likedImage = db.Like_infos
                .SingleOrDefault(v => v.username.CompareTo(User.Identity.Name) == 0 &&
                    v.images_id == id);

            bool isLiked = true;
            if (likedImage == null)
            {
                isLiked = false;
            }

            image.LikeCount = likeCount;
            image.CommentCount = commentCount;
            image.IsLiked = isLiked;

            return image;
        }

        /// <summary>
        /// Lấy danh sách bình luận của tấm ảnh
        /// </summary>
        /// <param name="images_id">ID của tấm ảnh</param>
        /// <param name="offset">Vị trí bắt đầu (mục đích phân trang)</param>
        /// <param name="limit">Số lượng bình luận cần lấy</param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("LayDanhSachBinhLuanCuaAnh")]
        [HttpGet, AcceptVerbs("GET")]
        public List<CommentsModels> LayDanhSachBinhLuanCuaAnh(int images_id, int offset, int limit)
        {
            List<CommentsModels> ds = new List<CommentsModels>();

            List<Comment_info> comments = db.Comment_infos
                .Where(c=>c.images_id == images_id)
                .ToList()
                .Skip((offset - 1) * limit).Take(limit).ToList();
            foreach(var item in comments)
            {
                CommentsModels cmt = new CommentsModels();

                cmt.Id = item.id;
                cmt.ImageId = item.images_id;
                cmt.Content = item.content;
                cmt.PostDate = item.postdate;

                UsersModels user = new UsersModels();
                user.Name = item.User.name;
                user.Username = item.User.username;
                user.UserType = (item.User.type == 1) ? UserTypeEnum.Administrator : UserTypeEnum.RegisteredUser;
                user.Avatar = item.User.avatar;
                user.About = item.User.about;
                user.Activation = item.User.activation;

                cmt.User = user;

                ds.Add(cmt);
            }

            return ds;
        }

        [Route("CapNhatThongTin")]
        [HttpPost, HttpGet, AcceptVerbs("POST", "GET")]
        public bool CapNhatThongTin(UsersModels user)
        {
            User userMD = (from b in db.Users
                           where b.username == HttpContext.Current.User.Identity.Name
                            select b).Single();
            //
            // code code code
            //
            userMD.name = user.Name;
            userMD.about = user.About;
            
            db.SubmitChanges();
            return true; // Cập nhật thành công
        }

        /// <summary>
        /// Thực hiện upload 1 tấm ảnh
        /// </summary>
        /// <returns></returns>
        [Route("UploadAnh")]
        [HttpPost, HttpGet, AcceptVerbs("GET", "POST")]
        public async Task<HttpResponseMessage> UploadAnh()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/Resources");
            var provider = new MultipartFormDataStreamProvider(root);
            string uploadedFilename = string.Empty;
            string user_dir = string.Empty;

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the file names.
                string post = provider.FormData["content"];
                foreach (MultipartFileData file in provider.FileData)
                {
                    //uploadedFilename += "\\" + HttpContext.Current.User.Identity.Name + "\\";
                    uploadedFilename += "\\" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-");
                    uploadedFilename += file.Headers.ContentDisposition.FileName.Replace('\"', ' ').Trim();

                    user_dir = root;
                    if (!Directory.Exists(user_dir))
                    {
                        Directory.CreateDirectory(user_dir);
                    }

                    //Rename file
                    File.Move(file.LocalFileName, user_dir + uploadedFilename);
                }

                Image img = new Image();
                img.content = post;
                img.link = uploadedFilename;
                img.username = HttpContext.Current.User.Identity.Name;
                img.postdate = DateTime.Now;

                db.Images.InsertOnSubmit(img);
                db.SubmitChanges();

                var res = Request.CreateResponse(HttpStatusCode.OK);

                //Luu link vao header
                res.Content = new StringContent(user_dir + uploadedFilename);
                res.StatusCode = HttpStatusCode.OK;
                res.Content.Headers.Expires = DateTime.Now.AddHours(4);
                res.Content.Headers.ContentType.MediaType = "text/plain";
                res.Content.Headers.Add("Filename", user_dir + uploadedFilename);

                return res;
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }


        //[Route("UpAnh")]
        //[HttpPost, HttpGet, AcceptVerbs("GET", "POST")]
        //public async Task<IList<FileDesc>> UpAnh()
        //{
        //    string PATH =  HttpContext.Current.Server.MapPath("~/Resources");
        //    List<FileDesc> result = new List<FileDesc>();
        //    if (Request.Content.IsMimeMultipartContent())
        //    {
        //        try
        //        {
        //            if (!Directory.Exists(PATH))
        //            {
        //                Directory.CreateDirectory(PATH);
        //            }

        //            MultipartFormDataStreamProvider stream = new MultipartFormDataStreamProvider(PATH);

        //            IEnumerable<HttpContent> bodyparts = await Request.Content.ReadAsMultipartAsync(stream);
        //            IDictionary<string, string> bodyPartFiles = stream.BodyPartFileNames;
        //            IList<string> newFiles = new List<string>();

        //            foreach (var item in bodyPartFiles)
        //            {
        //                var newName = string.Empty;
        //                var file = new FileInfo(item.Value);

        //                if (item.Key.Contains("\""))
        //                    newName = Path.Combine(file.Directory.ToString(), item.Key.Substring(1, item.Key.Length - 2));
        //                else
        //                    newName = Path.Combine(file.Directory.ToString(), item.Key);

        //                File.Move(file.FullName, newName);
        //                newFiles.Add(newName);
        //            }

        //            var uploadedFiles = newFiles.Select(i =>
        //            {
        //                var fi = new FileInfo(i);
        //                return new FileDesc(fi.Name, fi.FullName, fi.Length);
        //            }).ToList();

        //            result.AddRange(uploadedFiles);
        //        }
        //        catch (Exception e)
        //        {
        //        }
        //    }
        //    return result;
        //}

        [Route("DangAnh")]
        [HttpPost, HttpGet, AcceptVerbs("GET", "POST")]
        public int DangAnh([FromBody] ImagesModels img)
        {
            Image newImg = new Image();
            newImg.link = img.Link;
            newImg.username = img.User.Username;
            newImg.content = img.Content;
            newImg.postdate = img.PostDate;

            //For testing
            //newImg.link = "http://abc.com";
            //newImg.username = "d2phap";
            //newImg.content = "Bắt đầu một quyết tâm";
            //newImg.postdate = DateTime.Now;

            try
            {
                db.Images.InsertOnSubmit(newImg);
                db.SubmitChanges();
            }
            catch
            {
                return -1; // Loi
            }
            return newImg.id; // trả về mã bài vừa đăng
        }


        /// <summary>
        /// Thực hiện LIKE 1 tấm ảnh
        /// </summary>
        /// <param name="images_id">Mã tấm ảnh</param>
        /// <returns>Giá trị trả về là tổng số lượt like của tấm ảnh</returns>
        [Route("LikeAnh")]
        [HttpPost, HttpGet, AcceptVerbs("POST", "GET")]
        public int LikeAnh(int images_id)
        {
            Like_info like = new Like_info();
            like.username = HttpContext.Current.User.Identity.Name;
            like.images_id = images_id;

            try
            {
                db.Like_infos.InsertOnSubmit(like);
                db.SubmitChanges();
            }
            catch
            {
                return -1; //loi
            }
            int count_like = (from b in db.Like_infos
                              where b.images_id == images_id
                              select b.username).Count();
            return count_like;
        }

        /// <summary>
        /// Thực hiện UNLIKE 1 tấm ảnh
        /// </summary>
        /// <param name="images_id">Mã tấm ảnh</param>
        /// <returns>Giá trị trả về là tổng số lượt like của tấm ảnh</returns>
        [Route("UnlikeAnh")]
        [HttpDelete, HttpGet, AcceptVerbs("DELETE", "GET")]
        public int UnlikeAnh(int images_id)
        {
            Like_info like = db.Like_infos
                .SingleOrDefault(l => l.username == HttpContext.Current.User.Identity.Name && 
                    l.images_id == images_id);

            try
            {
                db.Like_infos.DeleteOnSubmit(like);
                db.SubmitChanges();
            }
            catch
            {
                return -1; //error
            }

            int CountLike = (from b in db.Like_infos
                             where b.images_id == images_id
                             select b.username).Count();

            return CountLike;
        }

        /// <summary>
        /// Thực hiện bình luận 1 tấm ảnh
        /// </summary>
        /// <param name="cmt">Nội dung bình luận</param>
        /// <returns>Giá trị trả về là tổng số lượt bình luận của tấm ảnh</returns>
        [Route("BinhLuanAnh")]
        [HttpPost, HttpGet, AcceptVerbs("POST", "GET")]
        //public int BinhLuanAnh(CommentsModels cmt)
        public int BinhLuanAnh(int images_id, string content)
        {
            Comment_info comment = new Comment_info();
            comment.username = HttpContext.Current.User.Identity.Name;
            comment.images_id = images_id;
            comment.content = content;
            comment.postdate = DateTime.Now;

            try
            {
                db.Comment_infos.InsertOnSubmit(comment);
                db.SubmitChanges();
            }
            catch
            {
                return -1;
            }

            int count_cmt = (from c in db.Comment_infos
                             where c.images_id == images_id
                             select c.id).Count();

            return count_cmt;// trả về tổng số lượt bình luận của tấm ảnh
        }

        /// <summary>
        /// Thực hiện XOÁ bình luận 1 tấm ảnh
        /// </summary>
        /// <param name="id">ID của bình luận</param>
        /// <returns>Giá trị trả về là tổng số lượt bình luận của tấm ảnh</returns>
        [Route("XoaBinhLuan")]
        [HttpDelete, HttpGet, AcceptVerbs("DELETE", "GET")]
        public int XoaBinhLuan(int id)
        {
            Comment_info comment = db.Comment_infos
                .SingleOrDefault(c => c.username == HttpContext.Current.User.Identity.Name &&
                c.id == id);

            try
            {
                db.Comment_infos.DeleteOnSubmit(comment);
                db.SubmitChanges();
            }
            catch
            {
                return -1;
            }

            int count_cmt = (from c in db.Comment_infos
                             where c.username == HttpContext.Current.User.Identity.Name
                             select c.id).Count();

            return count_cmt;// trả về tổng số lượt bình luận của tấm ảnh
        }

        /// <summary>
        /// Thực hiện xoá một tấm ảnh
        /// </summary>
        /// <param name="id">ID của tấm ảnh</param>
        /// <returns></returns>
        [Route("XoaAnh")]
        [HttpDelete, HttpGet, AcceptVerbs("DELETE", "GET")]
        public bool XoaAnh(int id)
        {
            var img = db.Images.SingleOrDefault(i => i.id == id);
            
            if(img != null)
            {
                try
                {
                    db.Images.DeleteOnSubmit(img);
                    db.SubmitChanges();

                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// Lấy danh sách users
        /// </summary>
        /// <param name="offset">Vị trí bắt đầu (mục đích phân trang)</param>
        /// <param name="limit">Số lượng ảnh cần lấy</param>
        /// <returns></returns>
        [Route("LayDanhSachUsers")]
        [HttpGet, AcceptVerbs("GET")]
        public List<UsersModels> LayDanhSachUsers(int offset, int limit)
        {
            List<UsersModels> usersMD = new List<UsersModels>();

            if (offset < 1) offset = 1;
            if (limit < 0) limit = 0;

            List<User> q = db.Users.Skip((offset - 1) * limit).Take(limit).ToList();
            for (int i = 0; i < q.Count; i++)
            {
                UsersModels user = new UsersModels();

                user.Username = q[i].username;
                user.Name = q[i].name;
                user.About = q[i].about;
                user.Avatar = q[i].avatar;
                user.UserType = (UserTypeEnum)q[i].type;
                user.Activation = q[i].activation;

                usersMD.Add(user);
            }

            return usersMD;
        }

        [Route("DangNhap")]
        [HttpPost, HttpGet, AcceptVerbs("POST", "GET")]
        public UsersModels DangNhapTaiKhoan(string username, string password)
        {
            dbPixieDataContext db = new dbPixieDataContext();

            var tmp = db.Users
                .SingleOrDefault(u => u.username.ToLower().CompareTo(username.ToLower()) == 0 &&
                    u.password.CompareTo(FunctionPixie.SHA1(password)) == 0);

            if (tmp == null)
            {
                return null;
            }

            UsersModels user = new UsersModels();

            user.Username = tmp.username;
            user.Name = tmp.name;
            user.About = tmp.about;
            user.Avatar = tmp.avatar;
            user.UserType = (UserTypeEnum)tmp.type;
            user.Activation = tmp.activation;

            HttpContext.Current.Session["user"] = user;
            return user;
        }

        [AllowAnonymous]
        [Route("DangKyTaiKhoan")]
        [HttpPost, HttpGet, AcceptVerbs("POST", "GET")]
        public bool DangKyTaiKhoan([FromBody]UsersModels user)
        {
            User u = new User();
            u.username = user.Username;
            u.password = FunctionPixie.SHA1(user.Password);
            u.facebookid = user.FacebookId;
            u.name = user.Name;
            u.about = user.About;
            u.avatar = user.Avatar;
            u.type = (user.UserType == UserTypeEnum.Administrator) ? 1 : 0;
            u.activation = user.Activation;

            // For test
            //u.username = "xxxxx";
            //u.password = FunctionPixie.SHA1("12345");
            //u.name = "Mùa xuân";
            //u.about = "Mùa xuân sang có hoa anh đào";
            //u.avatar = "không có";
            //u.type = 1;
            //u.activation = 1;

            try
            {
                db.Users.InsertOnSubmit(u);
                db.SubmitChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        [Route("TheoDoi")]
        [HttpPost, HttpGet, AcceptVerbs("POST", "GET")]
        public int TheoDoi(string name)
        {
            Follower follow = new Follower();
            follow.username = HttpContext.Current.User.Identity.Name;
            follow.follower_username = name;

            try
            {
                db.Followers.InsertOnSubmit(follow);
                db.SubmitChanges();

            }
            catch
            {
                return -1; //loi
            }

            int CountFollow = (from b in db.Followers
                               where b.username == HttpContext.Current.User.Identity.Name
                               select b.follower_username).Count();
            return CountFollow;
        }

        [Route("BoTheoDoi")]
        [HttpDelete, HttpGet, AcceptVerbs("DELETE", "GET")]
        public int BoTheoDoi(string name)
        {
            
            Follower follow = db.Followers
                .SingleOrDefault(l => l.username == HttpContext.Current.User.Identity.Name &&
                    l.follower_username == name);
            try
            {
                db.Followers.DeleteOnSubmit(follow);
                db.SubmitChanges();
            }
            catch
            {
                return -1; //error
            }

            int CountFollow = (from b in db.Followers
                               where b.username == HttpContext.Current.User.Identity.Name
                               select b.follower_username).Count();
            return CountFollow;
        }

        [Route("LayThongTin")]
        [HttpGet, AcceptVerbs("GET")]
        public UsersModels LayThongTin(string username)
        {
            UsersModels user = new UsersModels();
            User u = new User();
            u = (from b in db.Users
                 where b.username == username
                 select b).SingleOrDefault();

            user.Username = u.username;
            user.Name = u.name;
            user.About = u.about;
            user.Avatar = u.avatar;
            user.UserType = (UserTypeEnum)u.type;
            user.Activation = u.activation;

            return user;
        }

        /*
         * DEMO SỬ DỤNG OAUTH BÊN CLIENT *
         * 
            HttpClientHandler handler = new HttpClientHandler();
            handler.Credentials = new NetworkCredential("USERNAME", "HASHED_PASSWORD");
            HttpClient client = new HttpClient(handler);

            client.BaseAddress = new Uri("http://localhost:61608/");
            var response = client.GetAsync("api/TaiKhoan/LayThongTin").Result;

            ViewBag.kq = response.ToString();
        */

    }
}
