﻿using PixieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Syndication;
using System.Web.Http;

namespace PixieAPI.Controllers
{
    [RoutePrefix("api/RSS")]
    public class RSSController : ApiController
    {
        //DataContext để truy vấn LINQ
        private dbPixieDataContext db = new dbPixieDataContext();

        [Route("LayDanhSachAnhMoiNhat")]
        [HttpGet, AcceptVerbs("GET")]
        public Rss20FeedFormatter LayDanhSachAnhMoiNhat(int limit)
        {
            List<Image> images = db.Images
                .OrderByDescending(i => i.postdate)
                .Take(limit).ToList();

            List<SyndicationItem> items = new List<SyndicationItem>();

            foreach (var img in images)
            {
                SyndicationItem item = new SyndicationItem()
                {
                    Title = new TextSyndicationContent(img.id.ToString()),
                    Content = SyndicationContent.CreateHtmlContent(img.content),
                    BaseUri = new Uri(img.link),
                    PublishDate = new DateTimeOffset(img.postdate)
                };

                items.Add(item);
            }

            var feed = new SyndicationFeed("News feed - Pixie", 
                "Pixie RSS", 
                new Uri("http://" + Request.RequestUri.Authority + "/api/RSS/LayDanhSachAnhMoiNhat"),
                items)
            {
                Copyright = new TextSyndicationContent("Copyright (c) " + DateTime.Now.Year.ToString() + " Pixie"),
                Generator = "http://" + Request.RequestUri.Authority + "/api/RSS/LayDanhSachAnhMoiNhat"
            };

            return new Rss20FeedFormatter(feed);
        }
    }
}