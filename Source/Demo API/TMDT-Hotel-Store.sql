USE [TMDT-Hotel]
GO


/*---------- 1 ----------- LAY THONG TIN KHACH SAN*/

/*Thong tin cua khach san*/
create proc sp_ThongTinKhachSan
as
begin
	select id, Email, DiaChi, SĐT, TaiKhoanPaypal, KinhDo, ViDo
	from THONGTINKHACHSAN
end
go

/*Quy dinh - mo ta cua khach san*/
create proc sp_QuyDinh_Mota
	@MaHienThi char(2)
as
begin
	select qd_mt.MoTaKhachSan, qd_mt.QuyDinh
	from NGONNGU nn, QUYDINH_MOTA qd_mt
	where MaHienThi = @MaHienThi and nn.MaNgonNgu = qd_mt.MaNgonNgu
end
go

/*Danh sach tien nghi cua khach san*/
create proc sp_TienNghiKhachSan
	@MaHienThi char(2)
as
begin
	select tn_ks.MaTienNghiKS, tn_ks.TenTienNghiKS
	from NGONNGU nn, NGONNGU_TIENNGHIKHACHSAN tn_ks
	where nn.MaHienThi = @MaHienThi and tn_ks.MaNgonNgu = nn.MaNgonNgu
end
go



/*---------- 2 ---------- LAY THONG TIN PHONG*/

/*Danh sach cac loai phong*/
create proc sp_DanhSachLoaiPhong
	@MaHienThi char(2)
as
begin
	select lp.MaLoaiPhong, nn_lp.TenLoaiPhong, nn_lp.ThongTinPhong, 
			lp.MaLoaiPhong, lp.SoNguoiLonToiDa, lp.SoTreEmToiDa, lp.SoLuongPhong
	from NGONNGU nn, NGONNGU_LOAIPHONG nn_lp, LOAIPHONG lp
	where nn.MaHienThi = @MaHienThi 
		and nn_lp.MaNgonNgu = nn.MaNgonNgu 
		and lp.MaLoaiPhong = nn_lp.MaLoaiPhong
end
go


/*Thong tin chi tiet tung loai phong*/
create proc sp_ChiTietLoaiPhong
	@MaHienThi char(2), 
	@MaLoaiPhong int
as
begin
	select nn_lp.MaLoaiPhong, nn_lp.TenLoaiPhong, nn_lp.ThongTinPhong 
	from NGONNGU nn, NGONNGU_LOAIPHONG nn_lp
	where nn.MaHienThi = @MaHienThi 
		and nn_lp.MaNgonNgu = nn.MaNgonNgu
end
go

/*Danh sach tien nghi theo tung loai phong*/
create proc sp_DanhSachTienNghiLoaiPhong
	@MaHienThi char(2), 
	@MaLoaiPhong int 
as
begin
	select nn_tnp.TenTienNghiPhong
	from NGONNGU nn, NGONNGU_TIENNGHIPHONG nn_tnp, LOAIPHONG lp, TIENNGHI_LOAIPHONG tn_lp 
	where nn.MaHienThi = @MaHienThi 
		and nn_tnp.MaNgonNgu = nn.MaNgonNgu 
		and lp.MaLoaiPhong = @MaLoaiPhong 
		and tn_lp.MaLoaiPhong = lp.MaLoaiPhong 
		and nn_tnp.MaTienNghiPhong = tn_lp.MaTienNghi
end
go


/*---------- 3 ---------- LAY DANH SACH KHUYEN MAI*/

/*Thong tin co ban cac khuyen mai*/
create proc sp_ThongTinKhuyenMai
	@MaHienThi char(2)
as
begin
	select km.MaKhuyenMai, km.NgayBatDau, km.NgayKetThuc, ttkm.TieuDe
	from NGONNGU nn, THONGTINKHUYENMAI ttkm, KHUYENMAI km
	where nn.MaHienThi = @MaHienThi 
		and ttkm.MaNgonNgu = nn.MaNgonNgu 
		and ttkm.MaKhuyenMai = km.MaKhuyenMai 
		and km.NgayKetThuc > GETDATE()
end
go

/*Thong tin chi tiet mot khuyen mai*/
create proc sp_ThongTinChiTietKhuyenMai
	@MaHienThi char(2), 
	@MaKhuyenMai int
as
begin
	select km.MaKhuyenMai, km.NgayBatDau, km.NgayKetThuc, ttkm.TieuDe, ttkm.ThongTin 
	from NGONNGU nn, KHUYENMAI km, THONGTINKHUYENMAI ttkm
	where nn.MaHienThi = @MaHienThi 
		and km.MaKhuyenMai = @MaKhuyenMai
		and ttkm.MaNgonNgu = nn.MaNgonNgu
		and ttkm.MaKhuyenMai = km.MaKhuyenMai
end
go

/*Danh sach cac loai phong cua mot khuyen mai*/
create proc sp_DanhSachLoaiPhongKhuyenMai
	@MaKhuyenMai int
as
begin
	select km_lp.MaKhuyenMai, km_lp.MaLoaiPhong, km_lp.GiaKhuyenMai
	from KHUYENMAI_LOAIPHONG km_lp
	where km_lp.MaKhuyenMai = @MaKhuyenMai
end
go



