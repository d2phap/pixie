﻿using Hotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hotel.Controllers
{
    public class LoaiPhongController : ApiController
    {
        // GET api/loaiphong
        public IEnumerable<LOAIPHONG> Get()
        {
            HotelLINQDataContext db = new HotelLINQDataContext();

            return db.LOAIPHONGs.ToList();
        }

        //// GET api/loaiphong/1
        //public LOAIPHONG Get(int id)
        //{
        //    HotelLINQDataContext db = new HotelLINQDataContext();
        //    LOAIPHONG l = db.LOAIPHONGs.SingleOrDefault(v => v.MaLoaiPhong == id);

        //    return l;
        //}

        //// POST api/loaiphong
        //public void Post([FromBody]LOAIPHONG l)
        //{
        //    HotelLINQDataContext db = new HotelLINQDataContext();
        //    db.LOAIPHONGs.InsertOnSubmit(l);
        //    db.SubmitChanges();
        //}

        //// PUT api/loaiphong/1
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/loaiphong/1
        //public void Delete(int id)
        //{

        //}





        //
        // PHƯƠNG THỨC TUỲ CHỈNH API
        //


        /// <summary>
        /// Lấy LOAIPHONG theo id
        /// GET: api/loaiphong/laydulieu/4
        /// </summary>
        /// <param name="id">Mã loại phòng</param>
        /// <returns></returns>
        [HttpGet, ActionName("laydulieu")]
        [AcceptVerbs("GET")]
        public LOAIPHONG LayDuLieu(int id)
        {
            HotelLINQDataContext db = new HotelLINQDataContext();
            LOAIPHONG l = db.LOAIPHONGs.SingleOrDefault(v => v.MaLoaiPhong == id);

            return l;
        }

        /// <summary>
        /// api/loaiphong/sua
        /// </summary>
        /// <param name="id">Mã loại phòng</param>
        /// <param name="soluong">Số lượng trẻ em</param>
        /// <returns></returns>
        [HttpPut, ActionName("sua")]
        [AcceptVerbs("PUT")]
        public bool SuaLoaiPhong([FromBody]LOAIPHONG l)
        {
            HotelLINQDataContext db = new HotelLINQDataContext();
            LOAIPHONG l2 = db.LOAIPHONGs.SingleOrDefault(v => v.MaLoaiPhong == l.MaLoaiPhong);

            l2.SoNguoiLonToiDa = l.SoNguoiLonToiDa;
            l2.SoTreEmToiDa = l.SoTreEmToiDa;
            l2.SoLuongPhong = l.SoLuongPhong;
            l2.GiaPhong = l.GiaPhong;

            try
            {
                db.SubmitChanges();

                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// api/loaiphong/xoa/4
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete, ActionName("xoa")]
        [AcceptVerbs("GET", "DELETE")]
        public bool XoaLoaiPhong(int id)
        {
            HotelLINQDataContext db = new HotelLINQDataContext();
            LOAIPHONG l = db.LOAIPHONGs.SingleOrDefault(v => v.MaLoaiPhong == id);

            try
            {
                db.LOAIPHONGs.DeleteOnSubmit(l);
                db.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// api/loaiphong/them
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        [HttpPost, ActionName("them")]
        [AcceptVerbs("POST")]
        public bool ThemLoaiPhong([FromBody]LOAIPHONG l)
        {
            HotelLINQDataContext db = new HotelLINQDataContext();
            
            try
            {
                db.LOAIPHONGs.InsertOnSubmit(l);
                db.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

    }
}
