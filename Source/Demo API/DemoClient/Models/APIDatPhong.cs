﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class APIDatPhong
{
    public DateTime NgayDen { get; set; }
    public DateTime NgayDi { get; set; }
    public string GhiChu { get; set; }
    public decimal TongTien { get; set; }
    public string TaiKhoanPaypal { get; set; }
    public string HoTenNguoiNhanPhong { get; set; }
    public string SDTLienLac { get; set; }
    public List<APIChiTietDatPhong> ChiTietDatPhong { get; set; }
    public APIKhachHang KhachHang { get; set; }
}