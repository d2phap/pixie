﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class APIChiTietDatPhong
{
    public int MaLoaiPhong { get; set; }
    public int SoLuong { get; set; }
    public decimal GiaPhong { get; set; }
    public decimal ThanhTien { get; set; }
}