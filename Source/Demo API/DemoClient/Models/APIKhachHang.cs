﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class APIKhachHang
{
    public string HoTen { get; set; }
    public string SDT { get; set; }
    public string Email { get; set; }
    public string SoCaNhan { get; set; }
    public bool LoaiSoCaNhan { get; set; }
}