﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using DemoClient.Models;

namespace DemoClient.Controllers
{
    public class SuaController : Controller
    {
        //
        // GET: /LoaiPhong/

        public ActionResult Index(int id)
        {
            return View();
        }

        public ActionResult Sua(int id)
        {
            dbHotelDataContext db = new dbHotelDataContext();
            LOAIPHONG l = db.LOAIPHONGs.SingleOrDefault(v => v.MaLoaiPhong == id);

            ViewBag.loaiphong = l;
            return PartialView("~/Views/LoaiPhong/Index.cshtml");
        }

        [HttpPost]
        public void ThucHienSua(int txtMaLoaiPhong, int txtSoNguoiLon, int txtSoTreEm, 
            int txtSoLuongPhong, int txtGiaPhong)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:53792/");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            LOAIPHONG l = new LOAIPHONG();
            l.MaLoaiPhong = txtMaLoaiPhong;
            l.SoNguoiLonToiDa = txtSoNguoiLon;
            l.SoTreEmToiDa = txtSoTreEm;
            l.SoLuongPhong = txtSoLuongPhong;
            l.GiaPhong = txtGiaPhong;

            HttpResponseMessage response = client.PutAsJsonAsync("api/loaiphong/sua/", l).Result;

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                bool kq = response.Content.ReadAsAsync<bool>().Result;

                Response.Write("Kết quả sửa: " + kq.ToString());
            }
        }

    }
}
