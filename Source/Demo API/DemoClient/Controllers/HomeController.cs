﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using DemoClient.Models;
using System.Net.Http.Formatting;

namespace DemoClient.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:53792/");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            // List all products.
            HttpResponseMessage response = client.GetAsync("api/loaiphong").Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                ViewBag.loaiphong = response.Content.ReadAsAsync<IEnumerable<LOAIPHONG>>().Result;
                
            }

            return View();
        }

        public void Xoa(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:53792/");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));


            HttpResponseMessage response = client.DeleteAsync("api/loaiphong/xoa/" + id.ToString()).Result;
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                bool kq = response.Content.ReadAsAsync<bool>().Result;

                Response.Write("Kết quả xoá: " + kq.ToString());
            }
        }

        [HttpPost]
        public void Them(int txtSoNguoiLon, int txtSoTreEm, int txtSoLuongPhong, int txtGiaPhong)
        {
            LOAIPHONG l = new LOAIPHONG();
            l.SoNguoiLonToiDa = txtSoNguoiLon;
            l.SoTreEmToiDa = txtSoTreEm;
            l.SoLuongPhong = txtSoLuongPhong;
            l.GiaPhong = txtGiaPhong;
            l.TinhTrangKhoa = false;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:53792/");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync("api/loaiphong/them", l).Result;


            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                bool kq = response.Content.ReadAsAsync<bool>().Result;

                Response.Write("Kết quả thêm: " + kq.ToString());
            }

        }


    }
}
