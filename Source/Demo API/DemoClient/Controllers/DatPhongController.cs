﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace DemoClient.Controllers
{
    public class DatPhongController : Controller
    {
        //
        // GET: /DatPhong/

        public ActionResult Index()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:52111/");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            APIDatPhong dp = new APIDatPhong();
            List<APIChiTietDatPhong> dsct = new List<APIChiTietDatPhong>();
            APIKhachHang kh = new APIKhachHang();

            kh.HoTen = "Lê Minh Trang";
            kh.LoaiSoCaNhan = true;
            kh.SDT = "01674710360";
            kh.SoCaNhan = "291053999";
            kh.Email = "lmtrang@gmail.com";

            dp.GhiChu = "ko hút thuốc lá";
            dp.HoTenNguoiNhanPhong = "Bùi bá Lộc";
            dp.KhachHang = kh;
            dp.NgayDen = DateTime.Parse("2/1/2014");
            dp.NgayDi = DateTime.Parse("2/5/2014");
            dp.SDTLienLac = "0987654321";
            dp.TaiKhoanPaypal = "d2phapkakak";
            dp.TongTien = 4000000;
            dp.ChiTietDatPhong = new List<APIChiTietDatPhong>();

            APIChiTietDatPhong ct = new APIChiTietDatPhong();
            ct.GiaPhong = 100000;
            ct.MaLoaiPhong = 1;
            ct.SoLuong = 2;
            ct.ThanhTien = 200000;
            dp.ChiTietDatPhong.Add(ct);

            ct = new APIChiTietDatPhong();
            ct.GiaPhong = 200000;
            ct.MaLoaiPhong = 4;
            ct.SoLuong = 4;
            ct.ThanhTien = 800000;
            dp.ChiTietDatPhong.Add(ct);
            

            // List all products.
            HttpResponseMessage response = client.PostAsJsonAsync("api/datphong/xulydatphong", dp).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                ViewBag.kq = response.Content.ReadAsAsync<int>().Result;

            }

            return View();
        }

    }
}
